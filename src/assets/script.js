// $(document).ready(function () {

// 	//Variables for working with Location, Temprature and Times
// 	var lat;
// 	var lon;
// 	var tempInF;
// 	var tempInC;
// 	var timeFormatted;

// 	//Quotes depending on the weather
// 	var weatherQuotes = {
// 		rain: "\"The best thing one can do when it's raining is to let it rain.\" -Henry Wadsworth Longfellow",
// 		clearDay: "\"Wherever you go, no matter what the weather, always bring your own sunshine.\" -Anthony J. D'Angelo",
// 		clearNight: "\"The sky grew darker, painted blue on blue, one stroke at a time, into deeper and deeper shades of night.\" -Haruki Murakami",
// 		snow: "\"So comes snow after fire, and even dragons have their ending!\" -J. R. R. Tolkien",
// 		sleet: "\"Then come the wild weather, come sleet or come snow, we will stand by each other, however it blow.\" -Simon Dach",
// 		wind: "\"Kites rise highest against the wind - not with it.\" -Winston Churchill",
// 		fog: "\"It is not the clear-sighted who rule the world. Great achievements are accomplished in a blessed, warm fog.\" -Joseph Conrad",
// 		cloudy: "\"Happiness is like a cloud, if you stare at it long enough, it evaporates.\" -Sarah McLachlan",
// 		partlyCloudy: "\"Try to be a rainbow in someone's cloud.\" -Maya Angelou",
// 		default: "\"The storm starts, when the drops start dropping When the drops stop dropping then the storm starts stopping.\"― Dr. Seuss "
// 	};

// 	function locateYou() {
// 		//Try to get users location using their IP adress automattically.
// 		//It's not very precise but It's a way to get users location even if
// 		//their browser doesn't support Geolocation or if they refuse to share it.
// 		var ipApiCall = "https://ipapi.co/json";
// 		$.getJSON(ipApiCall, function (ipData) {
// 			lat = ipData.latitude;
// 			lon = ipData.longitude;
// 			city = ipData.city
// 			//console.log(lat+" "+lon+"ip"); (For Debugginh)
// 			// yourAddress();
// 			getWeather();
// 		});

// 		//Try to get location from users browser (device).
// 		if (navigator.geolocation) {
// 			navigator.geolocation.getCurrentPosition(function (position) {
// 				lat = position.coords.latitude;
// 				lon = position.coords.longitude;
// 				// console.log(lat+" "+lon+"geo"); (For Debugging)
// 				// yourAddress();
// 				getWeather();
// 			});
// 		}
// 	}

// 	//After collecting the Latiture and Longitute, Getting their formatted address from Google Maps.
// 	function yourAddress() {
// 		var googleApiCall = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon;
// 		$.getJSON(googleApiCall, function (locationName) {
// 			if (locationName.results[2] == undefined) {
// 				$('.api--error').addClass('shown');
// 			}
// 			$(".locName").html(locationName.results[2].formatted_address);

// 		});
// 	}

// 	function getWeather() {
// 		//Looking up the weather from Darkskies using users latitude and longitude.
// 		//Please don't use this API key. Get your own from DarkSkies.
// 		var weatherApiKey = "ea235b45d2188e096949765f6a12d098";
// 		// var weatherApiCall = "api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + weatherApiKey + "";
// 		var weatherApiCall = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&appid=${weatherApiKey}`;
// 		console.log(weatherApiCall);
// 		// var weatherApiCall = "https://api.darksky.net/forecast/"+weatherApiKey+"/"+lat+","+lon+"?units=si";
// 		$.ajax({
// 			url: weatherApiCall,
// 			type: "GET",
// 			dataType: "jsonp",
// 			success: function (weatherData) {
// 				// PRINT TO CONSOLE ON SUCCESS
// 				console.log(weatherData)
// 				//Fetching all the infor from the JSON file and plugging it into UI
// 					$(".locName").html(weatherData["name"]);
// 					$(".currentTemp").html((weatherData["main"]["temp"]).toFixed(1));
// 					$(".weatherCondition").html(weatherData["weather"][0]["main"]);
// 					$(".feelsLike").html((weatherData["main"]["feels_like"]).toFixed(1) + " °C");
// 					$(".humidity").html((weatherData["main"]["humidity"]));
// 					$(".windSpeed").html((weatherData["wind"]["speed"]));
// 				// $("");
// 				// $(".todaySummary").html(weatherData.hourly.summary);
// 				// $(".tempMin").html((weatherData["main"]["temp_min"]).toFixed(1) + " °C");
// 				// $(".tempMax").html((weatherData["main"]["temp_max"]).toFixed(1) + " °C");

// 				// $(".cloudCover").text((weatherData.currently.cloudCover * 100).toFixed(1) + " %");
// 				// $(".dewPoint").text(weatherData.currently.dewPoint + " °F");

// 				//Skycon Icons
// 				// skycons.set("weatherIcon", weatherData["weather"][0]["icon"]);
// 				// skycons.set("expectIcon", weatherData.hourly.icon);
// 				// skycons.play();
// 				//Load Quotes
// 				// var selectQuote = weatherData.currently.icon;
// 				// var loadQuote = $(".quote");

// 				// if ($(".weatherCondition")[0].innerText) {
// 				// 	$("body").css('background', 'url("/img/rain.jpg")');
// 				// }

// 				switch ($(".weatherCondition")[0].innerText) {
// 					case 'Rain':
// 						$("body").css('background', 'url("/img/rain.jpg")');
// 						break;
// 					case 'kAfasgoiajogifjrasoijs':
// 						break;
// 					default: 
// 						return;
// 				}

							

// 				// switch (weatherData.currently.icon) {
// 				// 	case "clear-day":
// 				// 		$(".quote").text(weatherQuotes.clearDay);
// 				// 		break;
// 				// 	case "clear-night":
// 				// 		$(".quote").text(weatherQuotes.clearNight);
// 				// 		break;
// 				// 	case "rain":
// 				// 		$(".quote").text(weatherQuotes.rain);
// 				// 		break;
// 				// 	case "snow":
// 				// 		$(".quote").text(weatherQuotes.snow);
// 				// 		break;
// 				// 	case "sleet":
// 				// 		$(".quote").text(weatherQuotes.sleet);
// 				// 		break;
// 				// 	case "clear-night":
// 				// 		$(".quote").text(weatherQuotes.clearNight);
// 				// 		break;
// 				// 	case "wind":
// 				// 		$(".quote").text(weatherQuotes.wind);
// 				// 		break;
// 				// 	case "fog":
// 				// 		$(".quote").text(weatherQuotes.fog);
// 				// 		break;
// 				// 	case "cloudy":
// 				// 		$(".quote").text(weatherQuotes.cloudy);
// 				// 		break;
// 				// 	case "partlyCloudy":
// 				// 		$(".quote").text(weatherQuotes.partlyCloudy);
// 				// 		break;
// 				// 	default:
// 				// 		$(".quote").text(weatherQuotes.default);
// 				// }
// 			}
// 		});
// 	}

// 	//Calling the function to locate user and fetch the data
// 	// locateYou();
// 	console.log(`LAT : ${lat}`);
// 	console.log(`LON : ${lon}`);

// 	//UI Tweaks
// 	$(".convertToggle").on("click", function () {
// 		$(".toggleIcon").toggleClass("ion-toggle-filled");
// 		var tmpNow = $(".currentTemp");
// 		var unit = $(".unit");
// 		var feelsLike = $(".feelsLike");

// 		if (tmpNow.text() == tempInC) {
// 			tmpNow.text(tempInF);
// 			unit.text("°F");
// 			feelsLike.text(feelsLikeInF + " °F")
// 		} else {
// 			tmpNow.text(tempInC);
// 			unit.text("°C");
// 			feelsLike.text(feelsLikeInC + " °C")
// 		}
// 	});


// 	//Smooth Scrool to Weekly Forecast section
// 	$(".goToWeek").on("click", function () {
// 		$('html, body').animate({
// 			scrollTop: $("#weeklyForecast").offset().top
// 		}, 1000);
// 	});


// 	//Google location Search
// 	function initialize() {
// 		var input = document.getElementById('locSearchBox');
// 		var autocomplete = new google.maps.places.Autocomplete(input);
// 		google.maps.event.addListener(autocomplete, 'place_changed', function () {
// 			var place = autocomplete.getPlace();
// 			lat = place.geometry.location.lat();
// 			lon = place.geometry.location.lng();
// 			//Calling the getWeather function to fetch data for Searched location
// 			getWeather();
// 		});
// 	}
// 	// google.maps.event.addDomListener(window, 'load', initialize);

// 	//Initiate wow.js
// 	new WOW().init();
// });