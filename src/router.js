import Vue from "vue";
import Router from "vue-router";
import weather from "./views/weather.vue";
import index from "./views/index.vue"
Vue.use(Router);

export default new Router({
    linkExactActiveClass: "active",
    routes: [{
            path: "/",
            name: "index",
            meta: { title: "Home" },
            components: {
                default: index,
            }
        },
        {
            path: "/weather",
            name: "weather",
            meta: { title: "Home" },
            components: {
                default: weather,
            }
        },
    ],


    scrollBehavior: to => {
        if (to.hash) {
            return { selector: to.hash };
        } else {
            return { x: 0, y: 0 };
        }
    }
});